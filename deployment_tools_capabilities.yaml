---
version: 0.1.0
capabilities:

  - category: starts-from
    description: >
      What needs to happen before the deployment tool takes over.
    tags:
      - name: bare-metal
        desc: Starts from scratch

      - name: os-installed
        desc: Needs a base operating system

      - name: env-bootstrap
        desc: Needs basic environment bootstrap (e.g. Docker, Puppet agent...)

      - name: kubernetes-cluster
        desc: Needs a full Kubernetes cluster available

  - category: technology
    description: >
      What base technologies the deployment tool is using.
    tags:
      # Packaging technologies
      - name: deb-packages
        desc: Makes use of Debian-style packaging

      - name: rpm-packages
        desc: Makes use of RPM-style packaging

      - name: oci-containers
        desc: Makes use of OCI containers

      - name: source-tarballs
        desc: Works directly from upstream-released source code tarballs

      - name: git
        desc: Works directly from git source code repositories

      # Base distribution
      - name: redhat-centos
        desc: Based on RedHat Enterprise Linux or CentOS

      - name: fedora
        desc: Based on Fedora

      - name: sles-opensuse
        desc: Based on SUSE Linux Enterprise Server or OpenSUSE

      - name: debian
        desc: Based on Debian GNU/Linux

      - name: ubuntu
        desc: Based on Ubuntu Server

      - name: oraclelinux
        desc: Based on Oracle Linux

      # Configuration management and orchestration technologies

      - name: puppet
        desc: Makes use of Puppet configuration management

      - name: chef
        desc: Makes use of Chef configuration management

      - name: ansible
        desc: Makes use of Ansible for configuration management

      - name: juju
        desc: Makes use of Juju for orchestration

      - name: helm
        desc: Makes use of Kubernetes Helm for orchestration

      - name: kubernetes
        desc: Makes use of Kubernetes for orchestration

      - name: heat
        desc: Uses OpenStack Heat for orchestration

  - category: components
    description: >
      Components of OpenStack supported by the deployment tool (latest version)
    tags:
      # Base services being deployed
      - name: message-queue
        desc: Deploys the OpenStack message queue (RabbitMQ...)

      - name: database
        desc: Deploys the OpenStack database (MySQL...)

      - name: secrets-storage
        desc: Deploys a Castellan-compatible secrets storage solution

      - name: etcd
        desc: Deploys etcd to enable advanced coordination capabilities

      - name: coordination
        desc: Deploys a tooling to enable advanced coordination capabilities (zookeeper, redis...)

      - name: cache
        desc: Deploys a caching solution for Keystone (memcached...)

      # OpenStack services
      - name: keystone
      - name: placement
      - name: glance
      - name: barbican
      - name: neutron
      - name: octavia
      - name: designate
      - name: ironic
      - name: cyborg
      - name: swift
      - name: cinder
      - name: manila
      - name: nova
      - name: zun
      - name: magnum
      - name: trove
      - name: sahara
      - name: murano
      - name: freezer
      - name: solum
      - name: masakari
      - name: heat
      - name: mistral
      - name: senlin
      - name: zaqar
      - name: aodh
      - name: blazar
      - name: horizon
      - name: ec2api
      - name: ceilometer
      - name: monasca
      - name: panko
      - name: watcher
      - name: vitrage
      - name: rally
      - name: adjutant
      - name: cloudkitty

  - category: upgrade
    description: >
      What is the main supported (tested) upgrade path when using this
      deployment tool.
    tags:
      # Control plane downtime
      - name: offline
        desc: Upgrade is done offline, control plane is down during upgrade.

      - name: online
        desc: Upgrade is done online, control plane is up during upgrade.

      # Upgrade methodology
      - name: per-version
        desc: Need to bring each new version online before upgrading the next

      - name: fast-forward
        desc: Each upgrade is done separately but no need to bring it online

      - name: skip-level
        desc: Direct x to z version upgrade, no need for intermediary steps

  - category: features
    description: >
      Specific features of the deployment tools
    tags:
      - name: offline-installation
        desc: No need for an Internet connection during installation or upgrade

      - name: supports-heterogeneous-versions
        desc: Supports running heterogeneous versions of OpenStack components

      - name: encrypted-local-comms
        desc: Management plane uses encrypted communications between components

      - name: all-in-one
        desc: Supports installing all components on a single machine

      - name: nova-cells
        desc: Supports deploying multiple Nova cells
